import sublime
import sublime_plugin
from os import path, walk
import re
import itertools

INPUT_TITLE = "#! Find everything"
INPUT_TEXT = "Input here: "
DB_FILE_NAME = "find_everything.db"


class WordPos:
    def __init__(self, file_name, line):
        self.file_name = file_name
        self.line = line

g_all_words = {}
g_db_path = ""
g_ignore = []

g_finder_activated = False


def db_init(folder):
    g_db_path = path.join(folder, DB_FILE_NAME)


def db_store(word, pos):
    global g_all_words
    if word in g_all_words:
        g_all_words[word].append(pos)
    else:
        g_all_words[word] = [pos]


def db_get(word):
    global g_all_words
    ret = []
    if word in g_all_words:
        ret = g_all_words[word]
    return ret


def db_count_words():
    global g_all_words
    return len(g_all_words)


def matches(string):
    for rule in g_ignore:
        rex = "^" + rule.lower().replace(".", "\\.").replace("*", ".*") + "$"
        pattern = re.compile(rex)
        return pattern.search(string.lower())


class IndexEverythingCommand(sublime_plugin.WindowCommand):
    def run(self):
        if len(self.window.folders()) == 0:
            aview = self.window.active_view()
            if aview is not None and aview.file_name() != "":
                    folders = [path.dirname(aview.file_name())]
            else:
                sublime.status_message("Can't index: no folder")
        else:
            folders = self.window.folders()

        if len(folders) == 0:
            sublime.status_message("No folder to index")
            return

        view = self.window.new_file()
        view.set_scratch(True)
        edit = view.begin_edit()
        sublime.status_message("Indexing...")

        global g_ignore
        g_ignore = []
        s = sublime.load_settings('FindEverything.sublime-settings')
        set_val = s.get("ignore")
        if set_val is not None:
            g_ignore.extend(set_val)
        if s.get("ignore_standarts"):
            s = self.window.active_view().settings()
            set_val = s.get("folder_exclude_patterns")
            if set_val is not None:
                g_ignore.extend(set_val)
            set_val = s.get("file_exclude_patterns")
            if set_val is not None:
                g_ignore.extend(set_val)
            set_val = s.get("binary_file_patterns")
            if set_val is not None:
                g_ignore.extend(set_val)
        g_ignore.sort()
        g_ignore = list(k for k, _ in itertools.groupby(g_ignore))

        db_init(folders[0])

        for folder in folders:
            view.insert(edit, view.size(), folder + "\n")
            for dirname, dirnames, filenames in walk(folder):
                if matches(path.basename(dirname)):
                    continue
                for filename in filenames:
                    if matches(filename):
                        continue
                    current_file = path.join(dirname, filename)
                    #view.insert(edit, view.size(), current_file + "\n")
                    try:
                        with open(current_file, "r") as f:
                            line_num = 0
                            for line in f:
                                sline = line.strip()
                                words = sline.split()
                                TO_STRIP = " {}\\/.,!@#$%^&*()<>[]\r\n"
                                swords = [
                                    sword for sword in [
                                        word.strip(TO_STRIP) for word in words]
                                    if sword != ""]
                                for sword in swords:
                                    db_store(sword,
                                             WordPos(current_file, line_num))
                                line_num += 1
                    except IOError, err:
                        view.insert(edit, view.size(),
                                    "Can't work fith file " + current_file +
                                    ": " + str(err) + "\n")
        view.insert(edit, view.size(), str(db_count_words()) + "\n")
        view.end_edit(edit)
        sublime.status_message("Indexed")


class FindEverythingCommand(sublime_plugin.WindowCommand):
    def run(self):
        view = self.window.new_file()
        view.set_scratch(True)
        view.set_name(INPUT_TITLE)
        view.settings().set("word_wrap", False)

        global g_finder_activated
        g_finder_activated = True
        sublime.status_message("Finder activated")

        edit = view.begin_edit()

        view.insert(edit, view.size(), INPUT_TEXT + "\n")
        view.sel().clear()
        view.sel().add(view.text_point(0, len(INPUT_TEXT)))

        view.end_edit(edit)

g_last_input = ""


class FindEverythingListener(sublime_plugin.EventListener):
    def on_activated(self, view):
        if view.name() == INPUT_TITLE:
            global g_finder_activated
            g_finder_activated = True
            sublime.status_message("Finder activated")

    def on_deactivated(self, view):
        if view.name() == INPUT_TITLE:
            global g_finder_activated
            g_finder_activated = False
            sublime.status_message("Finder deactivated")

    def on_selection_modified(self, view):
        if not g_finder_activated:
            return

        intersects = view.sel()[0].intersects(
            sublime.Region(0, view.text_point(0, len(INPUT_TEXT))))
        if intersects or view.sel()[0].begin() == 0:
            view.sel().clear()
            view.sel().add(view.text_point(0, len(INPUT_TEXT)))

    def on_modified(self, view):
        if not g_finder_activated:
            return

        global g_last_input

        intersects = view.sel()[0].intersects(
            sublime.Region(0, view.text_point(0, len(INPUT_TEXT))))
        if intersects or view.sel()[0].begin() == 0:
            view.run_command("undo")

        input_string = view.substr(view.line(view.text_point(0, 0)))
        sinput = input_string[len(INPUT_TEXT):].strip().strip(
            "!@#$%^&*(){}<>[]")
        sublime.status_message("Finder " + sinput)

        if sinput == g_last_input:
            return

        g_last_input = sinput
        word_poss = db_get(sinput)

        if len(word_poss) > 0:
            edit = view.begin_edit()
            view.erase(edit,
                       sublime.Region(view.text_point(1, 0), view.size()))
            view.insert(edit, view.size(), "\n")
            view.insert(edit, view.size(),
                        "Results " + str(len(word_poss)) + "\n")
            for word_pos in word_poss:
                view.insert(edit, view.size(),
                            word_pos.file_name + ":" + str(word_pos.line)
                            + "\n")
            view.end_edit(edit)
